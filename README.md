# Slush Systems

Slush Systems creates relentlessly up to date container images.

## What does "relentlessly up to date" mean?

Slush Systems container images are built continuously, some as often as once
per hour, and published anytime the image contents change.

This has a few advantages:

1. You don't have to call `apt-get upgrade` or `dnf update` at the top of
your Dockerfiles; images are already as up to date as they can be when
you pull them
1. Stable software that doesn't see new releases very often (e.g. bind) still
has its supporting infrastructure (e.g. bash, glibc, openssl,
ca-certificates) updated continuously
1. As fixes land for vulnerabilities in your supporting software (e.g.
openssl, openssh) they are continuously integrated without any manual
intervention

### Do you just push identical images all the time?

No, images are only tagged and pushed to Docker Hub when there are legitimate
changes. Images define their own factors to determine if changes are
legitimate.

For example, the `slushsystems/fedora-base` image uses a SHA256 hash of all
installed packages and their versions. The hash changes when packages are
added, removed, or their version changes. This in turn triggers a tag to
`:latest` and a push to Docker Hub.

Intelligent tagging ensures that you only have to pull & cache new images
when strictly necessary.

For more information on how this all works, take a look at the [Service Image
Builder](https://gitlab.com/slush-systems/service-image-builder/blob/master/README.md).
SIB implements image building and intelligent tagging.

### Isn't it dangerous not to pin all my dependencies at specific versions?

It's rare to find projects that pin their system packages to specific
versions. Most projects just run `apt-get update && apt-get -y upgrade` at
the top of their Dockerfile. Slush Systems Container Images aims to make that
redundant by providing always up to date images. That way, all you have to
worry about is the software you're packaging.

In the event you do want to pin a system package, you absolutely still can by
installing the desired version during your `docker build`.

## Why did you build this?

This project was born of frustration. All I wanted to do was setup an
authoritative DNS server with Bind and orchestrate it through Docker. Bind
has been around for a long time and tends to have few but severe
vulnerabilities. This means that when a vulnerability is published, it's
absolutely critical you update your instances as fast as possible.

The [ventz/bind](https://hub.docker.com/r/ventz/bind) image looks like
exactly what I need, but there's only one problem: What if the author gets
sick? Or hit by a bus? Or loses interest in the project? Or, god forbid,
takes a vacation? In any of those cases, I'm stuck scrambling to fork the
build and get updated before getting compromised. I'd prefer it if the
updates could just flow automatically rather than waiting for a person to
push a commit to a repository before a new build would become available.

When a new vulnerability gets published for OpenSSL, OpenSSH, Nginx, Apache,
or really anything, I don't want to drop everything I'm working on and rush
to update my machines. I want the update to automatically deploy to my
staging environment, pass tests, and then automatically push out to my
production infrastructure. And I'd prefer all of that happen before I even
wake up and start coffee.

That's the reality Slush Systems is meant to enable: fully automatic &
continuously deployed infrastructure _without_ human interaction. So the next
time a [Heartbleed](https://en.wikipedia.org/wiki/Heartbleed) or
[Shellshock](https://en.wikipedia.org/wiki/Shellshock_(software_bug))
happens, you're safe before other organizations even know they're vulnerable.
